up: 
	docker-compose -f ./docker/docker-compose.yml -f ./docker/docker-compose.local.yml up -d 

build:
	docker build -f ./docker/Dockerfile .

stop:
	docker-compose -f ./docker/docker-compose.yml -f ./docker/docker-compose.local.yml stop

down:
	docker-compose -f ./docker/docker-compose.yml -f ./docker/docker-compose.local.yml down

rootshell:
	 docker exec -u root -it rfn_drupal /bin/bash

shell:
	make rootshell
	



